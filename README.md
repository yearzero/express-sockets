# Doodle Di Doodle Dum

This is ATM a very basic **barebone** example for connecting an
 Express server with sockets.io
 
- The top 50% of the screen is the doodler I've built
- The bottom 50% of the screen is a super basic chat box.
- no validation, encryption, token/OTL, busness logic.
- caching is mocked via a variable that holds the drawing history(bad and unoptimized,
 could produce memory leaks on large objects), should be connected over redis.
 
## Table of Contents

- [Installation](#installation)
- [Usage](#usage)

## Installation

Download or clone this repo

```
git clone https://yearzero@bitbucket.org/yearzero/express-sockets.git
```

## Usage

1. `npm install`
2. `nodemon index.js`
3. Open [localhost:3000](localhost:3000) 
4. open another instance of the same browser /in incognito / or a different browser on the same url.
 