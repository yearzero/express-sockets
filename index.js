const express = require('express');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const requestIp = require('request-ip');

let line_history = [];

app.use(express.static(__dirname + '/public'));

app.get('/', function(req, res){
    console.log(requestIp.getClientIp(req));
    res.sendFile(__dirname + '/index.html');

});

io.on('connection', function(socket){
    // socket.broadcast.emit('hi');
    console.log('a user connected');
    socket.on('disconnect', function(){
        console.log('user disconnected');
    });
    socket.on('chat message', function(msg,user){
        console.log('message: ' + msg, user);
        io.sockets.emit('new msg', msg,user)
    });
    socket.on('drawing', function (x, y) {
        io.sockets.emit('draw this', {x,y})
        console.log(x,y);
    });
    // first send the history to the new client
    for (var i in line_history) {
        socket.emit('draw_line', { line: line_history[i] } );
    }
    // add handler for message type "draw_line".
    socket.on('draw_line', function (data) {
        console.log(data);
        // add received line to history
        line_history.push(data.line);
        // send line to all clients
        io.emit('draw_line', { line: data.line, styles: data.styles});
    });
    socket.on('erase', function (data) {
        console.log('db erase data',data);
        line_history = [];
        io.emit('erase_client')
    });

    socket.on('user details', function (data) {
        console.log('user', data);
    });


});

http.listen(3000, function(){
    console.log('listening on *:3000');
});